![Logo](zigbee-logo.png)

# homebridge-zigbee

ZigBee Platform plugin for [HomeBridge](https://github.com/nfarina/homebridge)

[![npm version](https://img.shields.io/npm/v/homebridge-zigbee.svg?style=flat-square)](https://www.npmjs.com/package/homebridge-zigbee-v2)

## Description

This homebridge plugin uses a ZigBee [adapter/dongle](https://www.zigbee2mqtt.io/information/supported_adapters.html) to access ZigBee devices from Apple's HomeKit. 

## [Documentaion](./docs/README.md)

* [Website](https://www.zigbee2mqtt.io)
    * Getting started
    * Information of features, configuration, debugging, groups, a FAQ...
    * Running Zigbee2mqtt on other platrorms
    * How tos
    * Integration with other systems
    * Support

## License

Licensed under [MIT](LICENSE)
