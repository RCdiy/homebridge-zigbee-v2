const HomeKitDevice = require('../HomeKitDevice')

class IkeaTradfriMotionSensor extends HomeKitDevice {
  static get description() {
    return {
      model: 'TRADFRI motion sensor',
      manufacturer: 'IKEA of Sweden',
      name: 'IKEA TRADFRI',
    }
  }

  getAvailbleServices() {
    return [{
      name: 'Motion',
      type: 'MotionSensor',
      history: {
        type: 'motion',
        use: 'status',
      },
    }]
  }

  onDeviceReady() {
    this.zigbee.update({ status: 'online', joinTime: Math.floor(Date.now() / 1000) })
    this.mountServiceCharacteristic({
      endpoint: 1,
      cluster: 'msOccupancySensing',
      service: 'Motion',
      characteristic: 'MotionDetected',
      reportMinInt: 1,
      reportMaxInt: 60,
      reportChange: 1,
      parser: 'occupancy',
      get: null,
    })
  }
}

module.exports = IkeaTradfriMotionSensor
